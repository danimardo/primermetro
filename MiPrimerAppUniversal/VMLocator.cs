﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiPrimerAppUniversal
{

    

    public class VMLocator
    {
        private Lazy<Persona> mainViewModel;

        VMLocator()
        {
            mainViewModel = new Lazy<Persona>(() =>
                new Persona("Daniel", SexoPersona.Varón, new DateTime(1976, 2, 29), "ES", "http://u.looplr.com/7/01/5084fb1d11010_80066822751e01a7fb1017.jpg"));
        }

        public Persona MainViewModel
        {
            get
            {
                return mainViewModel.Value;
            }
        }

    }
}
