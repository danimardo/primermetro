﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;

namespace MiPrimerAppUniversal
{
    public class ComandoSaludo : ICommand
    {
        public ComandoSaludo()
        {
        }

        public bool CanExecute(object parameter)
        {
            if (parameter == null)
                return false;

            string param = parameter.ToString().ToLowerInvariant();

            if (param == "daniel")
                return true;
            else
                return false;
            
        }


        public event EventHandler CanExecuteChanged;

        public async void Execute(object parameter)
        {
            string message = string.Format("Hola Papa llamado {0}", parameter);
            MessageDialog dlg = new MessageDialog(message);
            await dlg.ShowAsync();
        }

        public void RaiseCanExecuteChanged()
        {
            if (CanExecuteChanged != null)
                CanExecuteChanged(null, new EventArgs());
        }
    }
}
