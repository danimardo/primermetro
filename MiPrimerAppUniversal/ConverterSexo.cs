﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace MiPrimerAppUniversal
{
    public class ConverterSexo : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (SexoPersona.Varón == (SexoPersona)value)
            {
                return "http://vignette2.wikia.nocookie.net/grimm/images/6/6f/Male_Symbol.png/revision/latest?cb=20120918225501";
            }
            else {
                return "http://laopiniondelsur.com/wp-content/uploads/2014/05/mujer-simbolo-morelia-michoacan-201120110808.jpg";
            }

                
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
