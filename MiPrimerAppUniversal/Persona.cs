﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;

namespace MiPrimerAppUniversal
{
   
    public enum SexoPersona { Mujer, Varón }
    
    public class Persona : VMBase
    {
        #region Propiedades
        private string nombre = null;
        private SexoPersona? sexo = null;
        private string strsexo = null;
        private DateTime? fechaNac = null;
        private string codigoPaisNac = null;
        private string imageURL = null;

        private Lazy<ComandoDelegado> sayHelloCommand;

        public ICommand SayHelloCommand
        {
            get { return sayHelloCommand.Value; }
        }
        public string Nombre
        {
            get { return nombre; }
            set
            {
                nombre = value;
                RaisePropertyChanged("Nombre");
                /* if (PropertyChanged != null) // Osea, si alguien se ha suscrito a él
                 {
                     PropertyChanged(this, new PropertyChangedEventArgs("Nombre"));
                 }*/

            }
        }
        public SexoPersona? Sexo
        {
            get { return sexo; }
            set {
                sexo = value;
                strsexo = sexo.ToString();
            }
        }

        public string strSexo
        {
            get
            {
                return sexo.ToString();
            }
                
            set {
                    strSexo = sexo.ToString();
                }
            } 

        public DateTime? FechaNac
        {
            get { return fechaNac; }
            set { fechaNac = value; }
        }
        public int? Edad
        {
            get
            {
                if (fechaNac != null)
                    return (int)
                    (DateTime.Today.Subtract(fechaNac.Value).TotalDays / 365.25);
                else
                    return null;
            }
        }
        public string CodigoPaisNac
        {
            get { return codigoPaisNac; }

            set
            {
                codigoPaisNac = value.ToUpper();
            }
        }

        public string ImageURL {
            get { return imageURL; }
            set {
                imageURL = value;
            } }

        // Comando
       

        #endregion

        #region Constructores
        public Persona() { }
        public Persona(string nombre, SexoPersona sexo)
        {
            this.Nombre = nombre;
            this.Sexo = sexo;
            sayHelloCommand = new Lazy<ComandoDelegado>(
                ()=> new ComandoDelegado(SayHelloCommandExecute, SayHelloCommandCanExecute));
        }
        public Persona(string nombre, SexoPersona sexo,
        DateTime fechaNac)
        : this(nombre, sexo)
        {
            this.FechaNac = fechaNac;
        }
        public Persona(string nombre, SexoPersona sexo,
        DateTime fechaNac, string codPaisNac)
        : this(nombre, sexo, fechaNac)
        {
            this.codigoPaisNac = codigoPaisNac;
        }

        public Persona(string nombre, SexoPersona sexo, DateTime fechaNac, string codPaisNac, string imageURL)
       : this(nombre, sexo, fechaNac,codPaisNac)
        {
            this.imageURL = imageURL;
        }

        #endregion
        #region Métodos
        public override string ToString()
        {
            return (nombre != null ? nombre : "ANONIMO") +
            (sexo != null ? (sexo.Value == SexoPersona.Mujer ?
            " (M)" : " (V)") : "") +
            (fechaNac != null ? " (" +
            Edad.Value.ToString("dd/MM/yyyy") + ")" : "") +
            (codigoPaisNac != null ? "(" + CodigoPaisNac + ")" :
            "");
        }


        public async void SayHelloCommandExecute()
        {
            string msg = string.Format("Hello {0}", Nombre);
            MessageDialog dlg = new MessageDialog(msg);
            await dlg.ShowAsync();
        }

        public bool SayHelloCommandCanExecute()
        {
            if (Nombre.ToLowerInvariant() == "dani")
                return true;
            return false;
        }
        #endregion
    }
}
